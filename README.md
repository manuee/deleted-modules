Provides a set of drush commands for dealing with Drupal modules which were removed from the file system without being previously uninstalled correctly.

## How to use it

1. Clone the repository somewhere where drush scans for commands in your environment.
2. Clear the drush cache `drush cc drush`

## Commands provided

### deleted-modules
 `drush deleted-modules` or `drush dm` will output the list of modules which were removed from the file system without having previously uninstalled them correctly. 
 
 See `drush help dm` for options.

### deleted-modules-cleanup
`drush deleted-modules-cleanup` or `drush dmc` will attempt at downloading, enabling, disabling and uninstalling the listed modules in an attempt to clean up the situation. 

This should only be done after prior testing and after having created a backup of the site.